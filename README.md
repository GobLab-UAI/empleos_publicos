# Análisis del Portal de Empleos Públicos de Chile

Este repositorio es un proyecto de investigación del Laboratorio de innovación pública (GobLab) de la Universidad Adolfó Ibañez

El objetivo de este proyecto es analizar los datos del portal web de "Empleos Públicos" (http://www.empleospublicos.cl/) cuyo fin es facilitar y dar mayor transparencia a la difusión de la oferta de empleos que realizan los servicios públicos y reparticiones del Estado, para la publicación de sus convocatorias, permitiendo optimizar y transparentar las postulaciones de funcionarios públicos.

Las publicaciones del portal (aviso pizarron) incluyen información sobre los requisitos del trabajo, así como características más descriptivas, como el ministerio, el tipo de vacante y el salario bruto. Cada publicación está "abierta", en "evaluación" o "finalizada", con la mayoría de las postulaciones en el estado "finalizado".

Estos anuncios de trabajo entregan información sobre el mercado laboral del sector público de Chile. Además, estos datos también se pueden vincular a otras bases de datos como los del presupuesto de cada ministerio. (http://www.dipres.gob.cl/).

Este repositorio presenta un breve analisis de los datos del portal de empleos públicos utilizando Python Pandas.

## Los Datos

Los datos utilizado son todas los anuncios de trabajo publicados por el sitio web portal de Empleos públicos, y cuya extracción va desde 2009 a 2018. 

Los datos contienen las siguientes variables:
- Ministerio
- Institución / Entidad
- Cargo
- Nº de Vacantes
- Área de Trabajo
- Región
- Ciudad
- Tipo de Vacante
- Calendarización del Proceso

## Análisis

El análisis se realiza a partir de los datos contenidos en el archivo "empleos_publicos/data/all_data.csv",luego se utiliza `public_employment / analysis / transformation_pipeline.py` para agregar cualquier cambio al csv (limpieza, transformaciones, etc.). La idea es simplemente agregar una lista serializada de transformación para tener un solo canal de manera que podamos ejecutar los datos de principio a fin.

Finalmente en el archivo `view_graphs.ipynb`, es posible ver los gráficos ya creados. Cada gráfico se define en el archivo `public_employment / analysis / produce_results.py`. Estas funciones están agrupadas por aquellas que realmente producen un gráfico y las que producen el marco de datos subyacente que se utiliza para representar. Esto se indica mediante `_plot` o` _df` como el sufijo de cada nombre de función.

